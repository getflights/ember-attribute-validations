import '@glint/environment-ember-loose';

import CompanyModel from 'test-app/models/company';
import ContactModel from 'test-app/models/contact';

// Catch-all for Ember Data
declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    company: CompanyModel;
    contact: ContactModel;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
  }
}
