import Model, { attr } from '@ember-data/model';
import type Store from '@ember-data/store';
import validation from '@getflights/ember-attribute-validations/decorators/validation';
import type ValidationService from '@getflights/ember-attribute-validations/services/validation';
import { acceptance } from '@getflights/ember-attribute-validations/validations/acceptance';
import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { setupIntl } from 'ember-intl/test-support';

module('Model Validation | acceptance', function (hooks) {
  setupTest(hooks);
  setupIntl(hooks, {
    'ember-attribute-validations.validation.acceptance':
      'acceptance-translation',
  });

  test('validate', async function (assert) {
    class DummyModel extends Model {
      @attr()
      @validation(acceptance())
      declare property: boolean | undefined;
    }

    this.owner.register('model:dummy', DummyModel);

    const store = this.owner.lookup('service:store') as Store;
    const dummy = store.createRecord('dummy') as DummyModel;

    assert.strictEqual(
      dummy.property,
      undefined,
      'property is undefined at first',
    );
    assert.notOk(dummy.errors.has('property'), 'There is no error on property');

    const validationService = this.owner.lookup(
      'service:validation',
    ) as ValidationService;

    assert.true(
      await validationService.validateModel(dummy),
      'Validation succeeds because undefined is valid',
    );
    assert.notOk(
      dummy.errors.has('property'),
      'There is no error on property because undefined is valid',
    );

    dummy.property = false;

    assert.false(
      await validationService.validateModel(dummy),
      'Validation fails because false is not valid',
    );
    assert.ok(
      dummy.errors.has('property'),
      'There is an error on property because false is not valid',
    );

    assert.deepEqual(
      dummy.errors.errorsFor('property'),
      [
        {
          attribute: 'property',
          message: 'acceptance-translation',
        },
      ],
      'The correct error is set on the property attribute',
    );
  });
});
