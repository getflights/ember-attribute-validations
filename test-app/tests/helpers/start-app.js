import Application from 'test-app/app';
import config from 'test-app/config/environment';
import { run } from '@ember/runloop';

export default function startApp(attrs) {
  var application;

  var attributes = Object.assign({}, config.APP);
  attributes = Object.assign(attributes, attrs); // use defaults, but you can override;

  run(function () {
    application = Application.create(attributes);
    application.setupForTesting();
    application.injectTestHelpers();
  });

  return application;
}
