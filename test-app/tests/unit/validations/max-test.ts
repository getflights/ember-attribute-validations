import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { max } from '@getflights/ember-attribute-validations/validations/max';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | max', function (hooks) {
  setupTest(hooks);

  test('passing invalid arguments', async function (assert) {
    assert.throws(
      // @ts-expect-error testing wrong uses
      () => max(),
      'calling max() without argument throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => max('notNumber'),
      'calling max() with invalid argument throws an error',
    );

    assert.throws(
      () => max(NaN),
      'calling max() with invalid argument throws an error',
    );
  });

  test('validate (200)', async function (assert) {
    const max200Validation = max(200);

    assert.strictEqual(
      max200Validation.name,
      'max',
      'max validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Non-numbers are "valid"
      ['notanumber', '12345'],
      [[], '[]'],
      [{}, '{}'],
      [true, 'true'],
      [false, 'false'],
      [NaN, 'NaN'],
      // Number <= 200
      [-Infinity, '-Infinity'],
      [-1, '-1'],
      [0, '0'],
      [0.1, '0.1'],
      [1, '1'],
      [199.99999999, '1.99999999'],
      [200, '2'],
    ];

    for (const [value, label] of valids) {
      assert.true(await max200Validation.test(value), `${label} is valid`);
    }

    const invalids = [
      [200.00000000001, '200.00000000001'],
      [300, '300'],
      [300000, '300000'],
      [Infinity, 'Infinity'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        max200Validation.test(value),
        (e: unknown) => {
          return (
            e instanceof ValidationFailure && e.parameters?.['max'] === 200
          );
        },
        `${label} is not valid and returns the correct ValidationFailure`,
      );
    }
  });
});
