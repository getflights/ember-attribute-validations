import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { lengthBetween } from '@getflights/ember-attribute-validations/validations/length-between';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | length-between', function (hooks) {
  setupTest(hooks);

  test('passing invalid arguments', async function (assert) {
    assert.throws(
      // @ts-expect-error testing wrong uses
      () => lengthBetween(),
      'calling lengthBetween() without arguments throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => lengthBetween('notNumber'),
      'calling lengthBetween() with invalid first argument throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => lengthBetween(NaN),
      'calling lengthBetween() with invalid first argument throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => lengthBetween(1),
      'calling lengthBetween() with 1 argument throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => lengthBetween(1, 'notNumber'),
      'calling lengthBetween() with invalid second argument throws an error',
    );

    assert.throws(
      () => lengthBetween(1, NaN),
      'calling lengthBetween() with invalid second argument throws an error',
    );
  });

  test('validate (2 and 4)', async function (assert) {
    const lengthBetween1and3Validation = lengthBetween(2, 4);

    assert.strictEqual(
      lengthBetween1and3Validation.name,
      'length-between',
      'length-between validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      ['', 'empty string'],
      // Items that don't have 'length' or 'size'
      [12345, '12345'],
      [{}, 'empty object'],
      [true, 'true'],
      [false, 'false'],
      // Lengths from 2 to 4
      [[1, 2], 'array of 2 item'],
      [[1, 2, 3], 'array of 3 items'],
      [[1, 2, 3, 4], 'array of 4 items'],
      [new Set([1, 2]), 'set of 2 item'],
      [new Set([1, 2, 3]), 'set of 3 items'],
      [new Set([1, 2, 3, 4]), 'set of 4 items'],
      [
        new Map([
          [1, 'item'],
          [2, 'item'],
          [3, 'item'],
        ]),
        'map of 3 items',
      ],
      [
        new Map([
          [1, 'item'],
          [2, 'value'],
          [3, 'value'],
          [4, 'value'],
        ]),

        'map of 4 items',
      ],
      ['12', 'string of 2 character'],
      ['123', 'string of 3 characters'],
      ['1234', 'string of 4 characters'],
      [{ length: 2 }, '{ length: 2 }'],
      [{ length: 3 }, '{ length: 3 }'],
      [{ length: 4 }, '{ length: 4 }'],
      [{ size: 2 }, '{ size: 2 }'],
      [{ size: 3 }, '{ size: 3 }'],
      [{ size: 4 }, '{ size: 4 }'],
    ];

    for (const [value, label] of valids) {
      assert.true(
        await lengthBetween1and3Validation.test(value),
        `${label} is valid`,
      );
    }

    // All else is invalid
    const invalids = [
      [[], 'empty array'],
      [[1], 'array of 1 item'],
      [[1, 2, 3, 4, 5], 'array of 5 items'],
      [new Set([]), 'empty set'],
      [new Set([1]), 'array of 1 item'],
      [new Set([1, 2, 3, 4, 5]), 'set of 5 items'],
      [new Map(), 'empty map'],
      [new Map([[1, 'value']]), 'map of 1 item'],
      [
        new Map([
          [1, 'value'],
          [2, 'value'],
          [3, 'value'],
          [4, 'value'],
          [5, 'value'],
        ]),

        'map of 5 items',
      ],
      ['1', 'string of 1 character'],
      ['12345', 'string of 5 characters'],
      [{ length: 0 }, '{ length: 0 }'],
      [{ length: 1 }, '{ length: 1 }'],
      [{ length: 5 }, '{ length: 5 }'],
      [{ size: 0 }, '{ size: 0 }'],
      [{ size: 1 }, '{ size: 1 }'],
      [{ size: 5 }, '{ size: 5 }'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        lengthBetween1and3Validation.test(value),
        (e: unknown) => {
          return (
            e instanceof ValidationFailure &&
            e.parameters?.['from'] === 2 &&
            e.parameters?.['to'] === 4
          );
        },
        `${label} and returns the correct ValidationFailure`,
      );
    }
  });
});
