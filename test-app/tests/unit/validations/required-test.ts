import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { required } from '@getflights/ember-attribute-validations/validations/required';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | required', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const requiredValidation = required();

    assert.strictEqual(
      requiredValidation.name,
      'required',
      'required validation has the correct name',
    );

    // Null, undefined or empty is not valid
    const invalids = [
      [null, 'null'],
      [undefined, 'undefined'],
      [[], 'empty array'],
      ['', 'empty string'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        requiredValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }

    // All else is valid
    const valids = [
      [0, '0'],
      ['0', '"0"'],
      ['string', '"string"'],
      [true, 'true'],
      [false, 'false'],
      [{}, '{}'],
      [['item'], '["item"]'],
    ];

    for (const [value, label] of valids) {
      assert.true(await requiredValidation.test(value), `${label} is valid`);
    }
  });
});
