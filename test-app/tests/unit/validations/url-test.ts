import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { url } from '@getflights/ember-attribute-validations/validations/url';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | url', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const urlValidation = url();

    assert.strictEqual(
      urlValidation.name,
      'url',
      'url validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      ['', 'undefined'],
      // Valid url
      ['http://domain.net'],
      ['http://domain.net.eu'],
      ['http://sub.domain.com'],
      ['https://domain.net/page.html'],
      ['https://domain.net.eu'],
      ['https://sub.domain.com'],
    ];

    for (const [value, label] of valids) {
      assert.true(
        await urlValidation.test(value),
        `${value ?? label} is valid`,
      );
    }

    const invalids = [
      // Not a valid url
      ['manda.lorian@gmail.com'],
      ['manda.loria.n@dev.dom.net'],
      // Not a string or number
      [false, 'false'],
      ['some value', '"some value"'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        urlValidation.test(value),
        ValidationFailure,
        `${label ?? label} is not valid`,
      );
    }
  });
});
