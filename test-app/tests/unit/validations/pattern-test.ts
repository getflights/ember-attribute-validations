import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { pattern } from '@getflights/ember-attribute-validations/validations/pattern';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | pattern', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const patternValidation = pattern(new RegExp('^A$'));

    assert.strictEqual(
      patternValidation.name,
      'pattern',
      'pattern validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      ['', 'empty string'],
      // Only 'A' is valid
      ['A', '"A"'],
    ];

    for (const [value, label] of valids) {
      assert.true(await patternValidation.test(value), `${label} is valid`);
    }

    const invalids = [
      ['B', '"B"'],
      ['1', '"1"'],
      [2, '2'],
      // Not a string or number
      [{}, 'object'],
      [[], 'array'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        patternValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
