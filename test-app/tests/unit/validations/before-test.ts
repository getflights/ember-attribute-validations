import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { before } from '@getflights/ember-attribute-validations/validations/before';
import { dateBefore } from '@getflights/ember-attribute-validations/validations/date-before';

module('Validation | before', function (hooks) {
  setupTest(hooks);

  test('before is a reexport of date-before', async function (assert) {
    assert.strictEqual(before, dateBefore);
  });
});
