import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { forbidden } from '@getflights/ember-attribute-validations/validations/forbidden';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | forbidden', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const forbiddenValidation = forbidden();

    assert.strictEqual(
      forbiddenValidation.name,
      'forbidden',
      'forbidden validation has the correct name',
    );

    // Null, undefined or empty is valid
    const valids = [
      [null, 'null'],
      [undefined, 'undefined'],
      [[], 'empty array'],
      ['', 'empty string'],
    ];

    for (const [value, label] of valids) {
      assert.true(await forbiddenValidation.test(value), `${label} is valid`);
    }

    // All else is invalid
    const invalids = [
      [0, '0'],
      ['0', '"0"'],
      ['string', '"string"'],
      [true, 'true'],
      [false, 'false'],
      [{}, '{}'],
      [['item'], '["item"]'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        forbiddenValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
