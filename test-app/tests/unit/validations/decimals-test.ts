import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { decimals } from '@getflights/ember-attribute-validations/validations/decimals';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | decimals', function (hooks) {
  setupTest(hooks);

  test('passing invalid arguments', async function (assert) {
    assert.throws(
      // @ts-expect-error testing wrong uses
      () => decimals(),
      'calling decimals() without arguments throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => decimals('notANumber'),
      'calling decimals() without a non-number argument throws an error',
    );

    assert.throws(
      () => decimals(NaN),
      'calling decimals() without an invalid number argument throws an error',
    );
  });

  test('validate (2)', async function (assert) {
    const decimals2Validation = decimals(2);

    assert.strictEqual(
      decimals2Validation.name,
      'decimals',
      'decimals validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Not a valid number
      ['123', 'string'],
      [NaN, 'NaN'],
      [{}, 'object'],
      [[], 'array'],
      // Maximum 2 decimals
      [1, '1'],
      [1.2, '1.2'],
      [1.23, '1.23'],
    ];

    for (const [value, label] of valids) {
      assert.true(await decimals2Validation.test(value), `${label} is valid`);
    }

    const invalids = [
      [1.234, '1.234'],
      [-1.234, '-1.234'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        decimals2Validation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
