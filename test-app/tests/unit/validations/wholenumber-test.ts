import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { wholenumber } from '@getflights/ember-attribute-validations/validations/wholenumber';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | wholenumber', function (hooks) {
  setupTest(hooks);

  test('validate (2)', async function (assert) {
    const wholenumberValidation = wholenumber();

    assert.strictEqual(
      wholenumberValidation.name,
      'wholenumber',
      'wholenumber validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      ['', 'empty string'],
      // Not a valid number
      ['123', 'string'],
      [NaN, 'NaN'],
      [{}, 'object'],
      [[], 'array'],
      // whole numbers
      [1, '1'],
      [12345, '12345'],
    ];

    for (const [value, label] of valids) {
      assert.true(await wholenumberValidation.test(value), `${label} is valid`);
    }

    const invalids = [
      [0.1, '0.1'],
      [-0.1, '-0.1'],
      [1.234, '1.234'],
      [-1.234, '-1.234'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        wholenumberValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
