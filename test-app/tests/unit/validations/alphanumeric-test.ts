import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { alphanumeric } from '@getflights/ember-attribute-validations/validations/alphanumeric';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | alphanumeric', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const alphanumericValidation = alphanumeric();

    assert.strictEqual(
      alphanumericValidation.name,
      'alphanumeric',
      'alphanumeric validation has the correct name',
    );

    // None
    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      ['', 'empty string'],
      // Alphanumeric
      ['abcdef', '"abcdef"'],
      ['ABCDEF', '"ABCDEF"'],
      ['111111', '"111111"'],
      ['A9c6de', '"A9c6de"'],
      [111111, '111111 (number)'],
    ];

    for (const [value, label] of valids) {
      assert.true(
        await alphanumericValidation.test(value),
        `${label} is valid`,
      );
    }

    // Not alphanumeric - strings
    const invalids = [
      // strings
      [' abc', 'string with spaces'],
      ['-abc', 'string with special characters (-)'],
      ['ab*c', 'string with special characters (*)'],
      ['dee/c', 'string with special characters (/)'],
      ['1.23', 'string with special characters (.)'],
      [1.23, 'number with decimal places'],
      [{}, '{}'],
      [[], '[]'],
      [NaN, 'NaN'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        alphanumericValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
