import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { number } from '@getflights/ember-attribute-validations/validations/number';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | number', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const numberValidation = number();

    assert.strictEqual(
      numberValidation.name,
      'number',
      'number validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null is valid'],
      [undefined, 'undefined is valid'],
      // Finite & valid numbers are valid
      [255, '255 is valid'],
      /* prettier-ignore */
      [255.00, '255.00 is valid'],
      [0xff, '0xff is valid'],
      [0b11111111, '0b11111111 is valid'],
      [0.255e3, '0.255e3 is valid'],
      // - Negative
      [-255, '-255 is valid'],
      /* prettier-ignore */
      [-255.00, '-255.00 is valid'],
      [-0xff, '-0xff is valid'],
      [-0b11111111, '-0b11111111 is valid'],
      [-0.255e3, '-0.255e3 is valid'],
    ];

    for (const [value, label] of valids) {
      assert.true(await numberValidation.test(value), `${label} is valid`);
    }

    // All else is invalid
    const invalids = [
      [NaN, 'NaN'],
      [Infinity, 'Infinity'],
      [-Infinity, '-Infinity'],
      // Non-numbers are invalid
      [{}, 'object'],
      [[], 'array'],
      [true, 'true'],
      [false, 'false'],
      ['string', '"string"'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        numberValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
