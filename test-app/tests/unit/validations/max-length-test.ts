import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { maxLength } from '@getflights/ember-attribute-validations/validations/max-length';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | max-length', function (hooks) {
  setupTest(hooks);

  test('passing invalid arguments', async function (assert) {
    assert.throws(
      // @ts-expect-error testing wrong uses
      () => maxLength(),
      'calling maxLength() without argument throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => maxLength('notNumber'),
      'calling maxLength() with invalid argument throws an error',
    );

    assert.throws(
      () => maxLength(NaN),
      'calling maxLength() with invalid argument throws an error',
    );
  });

  test('validate (2)', async function (assert) {
    const maxLength2Validation = maxLength(2);

    assert.strictEqual(
      maxLength2Validation.name,
      'max-length',
      'max-length validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      ['', 'empty string'],
      // items that don't have 'length' or 'size' are not validated
      [12345, '12345'],
      [{}, '{}'],
      [true, 'true'],
      [false, 'false'],
      // Length <= 2
      [[], 'empty array'],
      [[1, 2], 'array of 2 items'],
      [new Set([]), 'empty set'],
      [new Set([1, 2]), 'set of 2 items'],
      [new Map(), 'empty map'],
      [
        new Map([
          [1, 'item'],
          [2, 'item'],
        ]),

        'map of 2 items',
      ],
      ['', 'empty string'],
      ['12', 'string of 2 characters'],
      [{ length: 0 }, '{ length: 0 }'],
      [{ length: 2 }, '{ length: 2 }'],
      [{ size: 0 }, '{ size: 0 }'],
      [{ size: 2 }, '{ size: 2 }'],
    ];

    for (const [value, label] of valids) {
      assert.true(await maxLength2Validation.test(value), `${label} is valid`);
    }

    const invalids = [
      // All else is invalid
      [[1, 2, 3], 'array of 3 items'],
      [new Set([1, 2, 3]), 'set of 3 items'],
      [
        new Map([
          [1, 'value'],
          [2, 'value'],
          [3, 'value'],
        ]),
        'map of 3 items',
      ],
      ['123', 'string of 3 characters'],
      [{ length: 3 }, '{ length: 3 }'],
      [{ size: 3 }, '{ size: 3 }'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        maxLength2Validation.test(value),
        (e: unknown) => {
          return (
            e instanceof ValidationFailure && e.parameters?.['length'] === 2
          );
        },
        `${label} is not valid and returns the correct ValidationFailure`,
      );
    }
  });
});
