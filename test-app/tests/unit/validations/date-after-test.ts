import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { dateAfter } from '@getflights/ember-attribute-validations/validations/date-after';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';
import { add, startOfToday, sub } from 'date-fns';

module('Validation | date-after', function (hooks) {
  setupTest(hooks);

  test('passing invalid arguments', async function (assert) {
    assert.throws(
      // @ts-expect-error testing wrong uses
      () => dateAfter(),
      'calling dateAfter() without arguments throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => dateAfter('notDateOrDateFn'),
      'calling dateAfter() without a non-date argument throws an error',
    );
  });

  test('validate against date', async function (assert) {
    const referenceDate = startOfToday();
    const dateAfterValidation = dateAfter(referenceDate);

    assert.strictEqual(
      dateAfterValidation.name,
      'date-after',
      'date-after validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Non-dates
      ['abc', 'string'],
      [123, 'number'],
      [{}, 'object'],
      [[], 'array'],
      // After
      [add(referenceDate, { seconds: 1 }), '1 second after referenceDate'],
    ];

    for (const [value, label] of valids) {
      assert.true(await dateAfterValidation.test(value), `${label} is valid`);
    }

    // Not after
    const invalids = [
      [referenceDate, 'referenceDate'],
      [new Date(referenceDate.getTime()), 'same time as referenceDate'],
      [sub(referenceDate, { seconds: 1 }), '1 second after referenceDate'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        dateAfterValidation.test(value),
        (e: unknown) => {
          return (
            e instanceof ValidationFailure &&
            e.parameters?.['date'] === String(referenceDate)
          );
        },
        `${label} is not valid and returns the correct ValidationFailure`,
      );
    }
  });

  test('validate against date function', async function (assert) {
    const referenceDate = startOfToday();
    const dateFn = () => referenceDate;
    const dateAfterValidation = dateAfter(dateFn);

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Non-dates
      ['abc', 'string'],
      [123, 'number'],
      [{}, 'object'],
      [[], 'array'],
      // After
      [add(referenceDate, { seconds: 1 }), '1 second after referenceDate'],
    ];

    for (const [value, label] of valids) {
      assert.true(await dateAfterValidation.test(value), `${label} is valid`);
    }

    // Not after
    const invalids = [
      [referenceDate, 'referenceDate'],
      [new Date(referenceDate.getTime()), 'same time as referenceDate'],
      [sub(referenceDate, { seconds: 1 }), '1 second after referenceDate'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        dateAfterValidation.test(value),
        (e: unknown) => {
          return (
            e instanceof ValidationFailure &&
            e.parameters?.['date'] === String(referenceDate)
          );
        },
        `${label} is not valid and returns the correct ValidationFailure`,
      );
    }
  });
});
