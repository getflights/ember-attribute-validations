import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { minLength } from '@getflights/ember-attribute-validations/validations/min-length';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | min-length', function (hooks) {
  setupTest(hooks);

  test('passing invalid arguments', async function (assert) {
    assert.throws(
      // @ts-expect-error testing wrong uses
      () => minLength(),
      'calling minLength() without argument throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => minLength('notNumber'),
      'calling minLength() with invalid argument throws an error',
    );

    assert.throws(
      () => minLength(NaN),
      'calling minLength() with invalid argument throws an error',
    );
  });

  test('validate (2)', async function (assert) {
    const minLength2Validation = minLength(2);

    assert.strictEqual(
      minLength2Validation.name,
      'min-length',
      'min-length validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      ['', 'empty string'],
      // items that don't have 'length' or 'size' are not validated
      [12345, '12345'],
      [{}, 'empty object'],
      [true, 'true'],
      [false, 'false'],
      // Length >= 2
      [[1, 2], 'array of 2 items'],
      [[1, 2, 3], 'array of 2 items'],
      [new Set([1, 2]), 'set of 2 items'],
      [new Set([1, 2, 3]), 'set of 3 item'],
      [
        new Map([
          [1, 'item'],
          [2, 'item'],
        ]),
        'map of 2 items',
      ],
      [
        new Map([
          [1, 'item'],
          [2, 'item'],
          [3, 'item'],
        ]),
        'map of 3 items',
      ],
      ['12', 'string of 2 characters'],
      ['123', 'string of 3 characters'],
      [{ length: 2 }, '{ length: 2 }'],
      [{ length: 3 }, '{ length: 3 }'],
      [{ size: 2 }, '{ size: 2 }'],
      [{ size: 3 }, '{ size: 3 }'],
    ];

    for (const [value, label] of valids) {
      assert.true(await minLength2Validation.test(value), `${label} is valid`);
    }

    const invalids = [
      [[], 'empty array'],
      [[1], 'array of 1 item'],
      [new Set([]), 'empty set'],
      [new Set([1]), 'set of 1 item'],
      [new Map(), 'empty map'],
      [new Map([[1, 'value']]), 'map of 1 item'],
      ['1', 'string of 1 character'],
      [{ length: 0 }, '{ length: 0 }'],
      [{ length: 1 }, '{ length: 1 }'],
      [{ size: 0 }, '{ size: 0 }'],
      [{ size: 1 }, '{ size: 1 }'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        minLength2Validation.test(value),
        (e: unknown) => {
          return (
            e instanceof ValidationFailure && e.parameters?.['length'] === 2
          );
        },
        `${label} is not valid and returns the correct ValidationFailure`,
      );
    }
  });
});
