import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { date } from '@getflights/ember-attribute-validations/validations/date';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | date', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const dateValidation = date();

    assert.strictEqual(
      dateValidation.name,
      'date',
      'date validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Dates
      [new Date(), 'new Date()'],
      [new Date(2025, 0, 1, 1, 23, 45, 123), '01/01/2025 01:24:45.123'],
    ];

    for (const [value, label] of valids) {
      assert.true(await dateValidation.test(value), `${label} is valid`);
    }

    const invalids = [
      // Invalid date
      [new Date(NaN), 'new Date(NaN)'],
      [new Date('invalid'), 'new Date("invalid")'],
      // Not a date
      ['abc', 'string'],
      [123, 'number'],
      [{}, 'object'],
      [[], 'array'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        dateValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
