import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { negative } from '@getflights/ember-attribute-validations/validations/negative';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | negative', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const negativeValidation = negative();

    assert.strictEqual(
      negativeValidation.name,
      'negative',
      'negative validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Non-numbers are "valid"
      ['notanumber', '12345'],
      [[], 'array'],
      [{}, 'object'],
      [true, 'true'],
      [false, 'false'],
      // Number <= 0
      [0, '0'],
      [-0.000000001, '-0.000000001'],
      [-1, '-1'],
      [-500, '-500'],
      [-Infinity, '-Infinity'],
    ];

    for (const [value, label] of valids) {
      assert.true(await negativeValidation.test(value), `${label} is valid`);
    }

    const invalids = [
      [1, '1'],
      [100, '100'],
      [Infinity, 'Infinity'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        negativeValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
