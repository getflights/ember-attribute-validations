import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { min } from '@getflights/ember-attribute-validations/validations/min';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | min', function (hooks) {
  setupTest(hooks);

  test('passing invalid arguments', async function (assert) {
    assert.throws(
      // @ts-expect-error testing wrong uses
      () => min(),
      'calling min() without argument throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => min('notNumber'),
      'calling min() with invalid argument throws an error',
    );

    assert.throws(
      () => min(NaN),
      'calling min() with invalid argument throws an error',
    );
  });

  test('validate (500)', async function (assert) {
    const min500Validation = min(500);

    assert.strictEqual(
      min500Validation.name,
      'min',
      'min validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Non-numbers are "valid"
      ['notanumber', '12345'],
      [[], 'array'],
      [{}, 'object'],
      [true, 'true'],
      [false, 'false'],
      [NaN, 'NaN'],
      // Number >= 200
      [500.00000001, '500.00000001'],
      [500, '500'],
      [800, '800'],
      [Infinity, 'Infinity'],
    ];

    for (const [value, label] of valids) {
      assert.true(await min500Validation.test(value), `${label} is valid`);
    }

    // All else is invalid
    const invalids = [
      [499.999999999, '499.999999999'],
      [400, '400'],
      [0.1, '0.1'],
      [-1, '-1'],
      [-Infinity, '-Infinity'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        min500Validation.test(value),
        (e: unknown) => {
          return (
            e instanceof ValidationFailure && e.parameters?.['min'] === 500
          );
        },
        `${label} is not valid and returns the correct ValidationFailure`,
      );
    }
  });
});
