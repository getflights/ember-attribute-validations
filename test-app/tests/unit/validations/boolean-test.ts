import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { boolean } from '@getflights/ember-attribute-validations/validations/boolean';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | boolean', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const booleanValidation = boolean();

    assert.strictEqual(
      booleanValidation.name,
      'boolean',
      'boolean validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Boolean
      [0, '0'],
      [1, '1'],
      [true, 'true'],
      [false, 'false'],
    ];

    for (const [value, label] of valids) {
      assert.true(await booleanValidation.test(value), `${label} is valid`);
    }

    const invalids = [
      ['abc', 'string'],
      [123, 'number'],
      [{}, 'object'],
      [[], 'array'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        booleanValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });

  test('validate strict (only real booleans)', async function (assert) {
    const booleanValidation = boolean(true);

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Boolean
      [true, 'true'],
      [false, 'false'],
    ];

    for (const [value, label] of valids) {
      assert.true(await booleanValidation.test(value), `${label} is valid`);
    }

    const invalids = [
      // Strict
      [0, '0'],
      [1, '1'],
      // Not a boolean
      ['abc', 'string'],
      [123, 'number'],
      [{}, 'object'],
      [[], 'array'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        booleanValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
