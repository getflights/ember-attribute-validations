import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { dateBefore } from '@getflights/ember-attribute-validations/validations/date-before';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';
import { add, startOfToday, sub } from 'date-fns';

module('Validation | date-before', function (hooks) {
  setupTest(hooks);

  test('passing invalid arguments', async function (assert) {
    assert.throws(
      // @ts-expect-error testing wrong uses
      () => dateBefore(),
      'calling dateBefore() without arguments throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => dateBefore('notDateOrDateFn'),
      'calling dateBefore() without a non-date argument throws an error',
    );
  });

  test('validate against date', async function (assert) {
    const referenceDate = startOfToday();
    const dateBeforeValidation = dateBefore(referenceDate);

    assert.strictEqual(
      dateBeforeValidation.name,
      'date-before',
      'date-before validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Non-dates
      ['abc', 'string'],
      [123, 'number'],
      [{}, 'object'],
      [[], 'array'],
      // Before
      [sub(referenceDate, { seconds: 1 }), '1 second before referenceDate'],
    ];

    for (const [value, label] of valids) {
      assert.true(await dateBeforeValidation.test(value), `${label} is valid`);
    }

    const invalids = [
      [referenceDate, 'referenceDate'],
      [new Date(referenceDate.getTime()), 'same time as referenceDate'],
      [add(referenceDate, { seconds: 1 }), '1 second after referenceDate'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        dateBeforeValidation.test(value),
        (e: unknown) => {
          return (
            e instanceof ValidationFailure &&
            e.parameters?.['date'] === String(referenceDate)
          );
        },
        `${label} is not valid and returns the correct ValidationFailure`,
      );
    }
  });

  test('validate against date function', async function (assert) {
    const referenceDate = startOfToday();
    const dateFn = () => referenceDate;
    const dateBeforeValidation = dateBefore(dateFn);

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Non-dates
      ['abc', 'string'],
      [123, 'number'],
      [{}, 'object'],
      [[], 'array'],
      // Before
      [sub(referenceDate, { seconds: 1 }), '1 second before referenceDate'],
    ];

    for (const [value, label] of valids) {
      assert.true(await dateBeforeValidation.test(value), `${label} is valid`);
    }

    const invalids = [
      [referenceDate, 'referenceDate'],
      [new Date(referenceDate.getTime()), 'same time as referenceDate'],
      [add(referenceDate, { seconds: 1 }), '1 second after referenceDate'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        dateBeforeValidation.test(value),
        (e: unknown) => {
          return (
            e instanceof ValidationFailure &&
            e.parameters?.['date'] === String(referenceDate)
          );
        },
        `${label} is not valid and returns the correct ValidationFailure`,
      );
    }
  });
});
