import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { zero } from '@getflights/ember-attribute-validations/validations/zero';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | zero', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const zeroValidation = zero();

    assert.strictEqual(
      zeroValidation.name,
      'zero',
      'zero validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Non-numbers are "valid"
      ['', 'empty string'],
      ['notanumber', 'string'],
      [[], 'array'],
      [{}, 'object'],
      [true, 'true'],
      [false, 'false'],
      // Number < 0
      [0, 'zero'],
    ];

    for (const [value, label] of valids) {
      assert.true(await zeroValidation.test(value), `${label} is valid`);
    }

    const invalids = [
      [-0.00001, '-0.00001'],
      [0.00001, '0.00001'],
      [1, '1'],
      [-1, '1'],
      [100, '100'],
      [-100, '100'],
      [Infinity, 'Infinity'],
      [-Infinity, '-Infinity'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        zeroValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
