import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { inclusion } from '@getflights/ember-attribute-validations/validations/inclusion';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | inclusion', function (hooks) {
  setupTest(hooks);

  test('passing invalid arguments', async function (assert) {
    assert.throws(
      // @ts-expect-error testing wrong uses
      () => inclusion(),
      'calling inclusion() without arguments throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => inclusion('notAnArray'),
      'calling inclusion() without a non-array argument throws an error',
    );

    assert.throws(
      () => inclusion([]),
      'calling inclusion() without an empty array argument throws an error',
    );
  });

  test('validate (strings)', async function (assert) {
    const inclusionValidation = inclusion(['allowed', 'also_allowed']);

    assert.strictEqual(
      inclusionValidation.name,
      'inclusion',
      'inclusion validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // "allowed" and "also_allowed" are valid
      ['allowed', '"allowed"'],
      ['also_allowed', '"also_allowed"'],
    ];

    for (const [value, label] of valids) {
      assert.true(await inclusionValidation.test(value), `${label} is valid`);
    }

    const invalids = [
      // All else is invalid
      ['', 'empty string'],
      ['not_allowed', '"not_allowed"'],
      [123, '123'],
      [false, 'false'],
      [{}, 'object'],
      [[], 'array'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        inclusionValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });

  test('validate (numbers)', async function (assert) {
    const inclusionValidation = inclusion([12, 34]);

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // 12 and 34 are valid
      [12, '12'],
      [34, '34'],
    ];

    for (const [value, label] of valids) {
      assert.true(await inclusionValidation.test(value), `${label} is valid`);
    }

    const invalids = [
      // All else is invalid
      ['12', '"12"'],
      ['34', '"34"'],
      [56, '56'],
      ['not_allowed', '"not_allowed"'],
      [123, '123'],
      [false, 'false'],
      [[], '[]'],
      [{}, '{}'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        inclusionValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });

  test('validate (mixed)', async function (assert) {
    const inclusionValidation = inclusion([123, true, 'string']);

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // 123, true and "string" are valid
      [123, '123'],
      [true, 'true'],
      ['string', '"string"'],
    ];

    for (const [value, label] of valids) {
      assert.true(await inclusionValidation.test(value), `${label} is valid`);
    }

    const invalids = [
      // All else is invalid
      ['123', '"123"'],
      ['true', '"true"'],
      [56, '56'],
      ['not_allowed', '"not_allowed"'],
      [12345, '12345'],
      [false, 'false'],
      [[], '[]'],
      [{}, '{}'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        inclusionValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
