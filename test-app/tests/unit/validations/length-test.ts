import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { length } from '@getflights/ember-attribute-validations/validations/length';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | length', function (hooks) {
  setupTest(hooks);

  test('passing invalid arguments', async function (assert) {
    assert.throws(
      // @ts-expect-error testing wrong uses
      () => length(),
      'calling length() without argument throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => length('notNumber'),
      'calling length() with invalid argument throws an error',
    );

    assert.throws(
      () => length(NaN),
      'calling length() with invalid argument throws an error',
    );
  });

  test('validate (5)', async function (assert) {
    const length5Validation = length(5);

    assert.strictEqual(
      length5Validation.name,
      'length',
      'length validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      ['', 'empty string'],
      // items that don't have 'length' or 'size' are not validated
      [12345, '12345'],
      [{}, '{}'],
      [true, 'true'],
      [false, 'false'],
      // Length 5
      [[1, 2, 3, 4, 5], 'array of 5 items'],
      [new Set([1, 2, 3, 4, 5]), 'set of 5 items'],
      [
        new Map([
          [1, 'item'],
          [2, 'item'],
          [3, 'item'],
          [4, 'item'],
          [5, 'item'],
        ]),
        'map of 5 items',
      ],
      ['12345', 'string of 5 characters'],
      [{ length: 5 }, '{ length: 5 }'],
      [{ size: 5 }, '{ size: 5 }'],
    ];

    for (const [value, label] of valids) {
      assert.true(await length5Validation.test(value), `${label} is valid`);
    }

    const invalids = [
      [[], 'empty array'],
      [[1, 2, 3, 4], 'array of 4 items'],
      [[1, 2, 3, 4, 5, 6], 'array of 6 items'],
      [new Set([]), 'empty set'],
      [new Set([1, 2, 3, 4]), 'set of 4 items'],
      [new Set([1, 2, 3, 4, 5, 6]), 'set of 6 items'],
      [new Map(), 'empty map'],
      [
        new Map([
          [1, 'value'],
          [2, 'value'],
          [3, 'value'],
          [4, 'value'],
        ]),
        'map of 4 items',
      ],
      [
        new Map([
          [1, 'value'],
          [2, 'value'],
          [3, 'value'],
          [4, 'value'],
          [5, 'value'],
          [6, 'value'],
        ]),
        'map of 4 items',
      ],
      ['1234', 'string of 4 characters'],
      ['123456', 'string of 6 characters'],
      [{ length: 0 }, '{ length: 0 }'],
      [{ length: 4 }, '{ length: 4 }'],
      [{ length: 6 }, '{ length: 6 }'],
      [{ size: 0 }, '{ size: 0 }'],
      [{ size: 4 }, '{ size: 4 }'],
      [{ size: 6 }, '{ size: 6 }'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        length5Validation.test(value),
        (e: unknown) => {
          return (
            e instanceof ValidationFailure && e.parameters?.['length'] === 5
          );
        },
        `${label} is not valid and returns the correct ValidationFailure`,
      );
    }
  });
});
