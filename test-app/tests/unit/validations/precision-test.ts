import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { precision } from '@getflights/ember-attribute-validations/validations/precision';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | precision', function (hooks) {
  setupTest(hooks);

  test('passing invalid arguments', async function (assert) {
    assert.throws(
      // @ts-expect-error testing wrong uses
      () => precision(),
      'calling precision() without argument throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => precision('notNumber'),
      'calling precision() with invalid argument throws an error',
    );

    assert.throws(
      () => precision(NaN),
      'calling precision() with invalid argument throws an error',
    );
  });

  test('validate (3)', async function (assert) {
    const precision3Validation = precision(3);

    assert.strictEqual(
      precision3Validation.name,
      'precision',
      'precision validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Non-numbers are "valid"
      ['notanumber', '12345'],
      [[], 'array'],
      [{}, 'object'],
      [true, 'true'],
      [false, 'false'],
      // Number > 0 is valid
      [9, '9.99'],
      [99, '9.99'],
      [999, '999'],
      [99.9, '99.9'],
      [9.99, '9.99'],
    ];

    for (const [value, label] of valids) {
      assert.true(await precision3Validation.test(value), `${label} is valid`);
    }

    const invalids = [
      [9.999, '9.999'],
      [99.99, '99.99'],
      [999.9, '999.9'],
      [9999, '9999'],
      [Infinity, 'Infinity'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        precision3Validation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
