import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { positive } from '@getflights/ember-attribute-validations/validations/positive';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | positive', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const positiveValidation = positive();

    assert.strictEqual(
      positiveValidation.name,
      'positive',
      'positive validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Non-numbers are "valid"
      ['notanumber', '12345'],
      [[], 'array'],
      [{}, 'object'],
      [true, 'true'],
      [false, 'false'],
      // Number >= 0 is valid
      [0, '0'],
      [0.000000001, '-0.000000001'],
      [1, '1'],
      [500, '500'],
      [Infinity, 'Infinity'],
    ];

    for (const [value, label] of valids) {
      assert.true(await positiveValidation.test(value), `${label} is valid`);
    }

    // All else is invalid
    const invalids = [
      [-1, '-1'],
      [-100, '-100'],
      [-Infinity, '-Infinity'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        positiveValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
