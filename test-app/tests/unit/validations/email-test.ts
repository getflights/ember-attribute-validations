import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { email } from '@getflights/ember-attribute-validations/validations/email';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | email', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const emailValidation = email();

    assert.strictEqual(
      emailValidation.name,
      'email',
      'email validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      ['', 'empty string'],
      // Valid email
      ['franky@codeshare.be', '"franky@codeshare.be" is valid'],
      ['pj.carly@it.getflights.com', '"pj.carly@it.getflights.com" is valid'],
      ['myemail+sub@gmail.com', '"myemail+sub@gmail.com" is valid'],
    ];

    for (const [value, label] of valids) {
      assert.true(await emailValidation.test(value), `${label} is valid`);
    }

    const invalids = [
      // Not a valid email
      ['abc123@nodomain', '"abc123@nodomain"'],
      ['domain.com', '"domain.com"'],
      ['somevalue', '"somevalue"'],
      // Not a string or number
      [{}, 'object'],
      [[], 'array'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        emailValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
