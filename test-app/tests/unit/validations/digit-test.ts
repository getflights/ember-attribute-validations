import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { digit } from '@getflights/ember-attribute-validations/validations/digit';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | digit', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const digitValidation = digit();

    assert.strictEqual(
      digitValidation.name,
      'digit',
      'digit validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      ['', 'empty string'],
      // Only digits
      ['1', '"1"'],
      ['1', '"123"'],
      [1, '1'],
      [123, '123'],
    ];

    for (const [value, label] of valids) {
      assert.true(await digitValidation.test(value), `${label} is valid`);
    }

    const invalids = [
      // Not only digits
      ['abc123', '"abc123"'],
      ['1.234', '"1.234"'],
      [1.234, '1.234'],
      // Not a string or number
      [{}, 'object'],
      [[], 'array'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        digitValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
