import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { acceptance } from '@getflights/ember-attribute-validations/validations/acceptance';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | acceptance', function (hooks) {
  setupTest(hooks);

  test('validate', async function (assert) {
    const acceptanceValidation = acceptance();

    assert.strictEqual(
      acceptanceValidation.name,
      'acceptance',
      'acceptance validation has the correct name',
    );

    // None
    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Truthy
      [true, 'true'],
      [1, '1'],
      [-1, '-1'],
      [1n, '1n'],
      ['string', '"string"'],
      [[], 'array'],
      [{}, 'object'],
      [function () {}, 'function'],
    ];

    for (const [value, label] of valids) {
      assert.true(await acceptanceValidation.test(value), `${label} is valid`);
    }

    // Not truthy
    const invalids = [
      [false, 'false'],
      [0, '0'],
      ['', 'empty string'],
      [NaN, 'NaN'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        acceptanceValidation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
