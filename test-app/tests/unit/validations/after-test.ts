import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { after } from '@getflights/ember-attribute-validations/validations/after';
import { dateAfter } from '@getflights/ember-attribute-validations/validations/date-after';

module('Validation | after', function (hooks) {
  setupTest(hooks);

  test('after is a reexport of date-after', async function (assert) {
    assert.strictEqual(after, dateAfter);
  });
});
