import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { range } from '@getflights/ember-attribute-validations/validations/range';
import ValidationFailure from '@getflights/ember-attribute-validations/-private/validation-failure';

module('Validation | range', function (hooks) {
  setupTest(hooks);

  test('passing invalid arguments', async function (assert) {
    assert.throws(
      // @ts-expect-error testing wrong uses
      () => range(),
      'calling range() without arguments throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => range('notNumber'),
      'calling range() with invalid first argument throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => range(NaN),
      'calling range() with invalid first argument throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => range(1),
      'calling range() with 1 argument throws an error',
    );

    assert.throws(
      // @ts-expect-error testing wrong uses
      () => range(1, 'notNumber'),
      'calling range() with invalid second argument throws an error',
    );

    assert.throws(
      () => range(1, NaN),
      'calling range() with invalid second argument throws an error',
    );
  });

  test('validate (100 to 200)', async function (assert) {
    const range100To200Validation = range(100, 200);

    assert.strictEqual(
      range100To200Validation.name,
      'range',
      'range validation has the correct name',
    );

    const valids = [
      // None
      [null, 'null'],
      [undefined, 'undefined'],
      // Non-numbers are "valid"
      ['notanumber', '12345'],
      [[], 'array'],
      [{}, 'object'],
      [true, 'true'],
      [false, 'false'],
      [NaN, 'NaN'],
      // Number between 100 and 200 is valid
      [100, '100'],
      [131, '131'],
      [175.758569558, '175.758569558'],
      [200, '200'],
    ];

    for (const [value, label] of valids) {
      assert.true(
        await range100To200Validation.test(value),
        `${label} is valid`,
      );
    }

    // All else is invalid
    const invalids = [
      [-Infinity, '-Infinity'],
      [0, '0'],
      [99.999999, '99.999999'],
      [200.000001, '200.000001'],
      [200.000001, '300'],
      [Infinity, 'Infinity'],
    ];

    for (const [value, label] of invalids) {
      await assert.rejects(
        range100To200Validation.test(value),
        ValidationFailure,
        `${label} is not valid`,
      );
    }
  });
});
