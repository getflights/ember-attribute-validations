import { setupTest } from 'ember-qunit';
import { module, skip } from 'qunit';
import { type TestContext } from '@ember/test-helpers';

interface ObjectValidationTestContext extends TestContext {}

module('Object Validation', function (hooks) {
  setupTest(hooks);

  skip('TODO: Validation for normal JS objects', function (this: ObjectValidationTestContext, assert) {
    assert.expect(0);
  });
});
