import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';
import { type TestContext } from '@ember/test-helpers';
import Service from '@ember/service';
import Model from '@ember-data/model';
import ValidationModel from '@getflights/ember-attribute-validations/models/validation-model';
import Store from '@ember-data/store';

interface ValidationModelTestContext extends TestContext {
  validation: ValidationServiceStub;
  store: Store;
  instance: SomeModel;
}

class ValidationServiceStub extends Service {
  lastValidatedModel?: Model;

  validateModel(model: Model) {
    this.lastValidatedModel = model;
    return true;
  }
}

class SomeModel extends ValidationModel {}

module('Model | ValidationModel', function (hooks) {
  setupTest(hooks);

  hooks.beforeEach(function (this: ValidationModelTestContext, assert) {
    this.owner.register('service:validation', ValidationServiceStub);
    this.validation = this.owner.lookup(
      'service:validation',
    ) as ValidationServiceStub;
    this.validation.lastValidatedModel = undefined;

    this.store = this.owner.lookup('service:store') as Store;
    this.owner.register('model:some', SomeModel);

    // Initial state
    this.validation.lastValidatedModel = undefined;
    assert.strictEqual(
      this.validation.lastValidatedModel,
      undefined,
      '(pre-test) The lastValidatedModel is undefined at first',
    );

    this.instance = this.store.createRecord('some');
    assert.notStrictEqual(
      this.instance,
      undefined,
      '(pre-test) this.instance is not undefined',
    );
    assert.ok(
      this.instance instanceof SomeModel,
      '(pre-test) this.instance instanceof SomeModel',
    );
  });

  test('this.validate() does the same as this.validation.validateModel(this)', function (this: ValidationModelTestContext, assert) {
    assert.strictEqual(
      typeof this.instance.validate,
      'function',
      '.validate is a function',
    );
    assert.ok(
      this.instance.validate(),
      '.validate() returns true (hardcoded in stub service)',
    );
    assert.strictEqual(
      this.validation.lastValidatedModel,
      this.instance,
      'The lastValidatedModel on the stub validation service is our model.',
    );
  });

  test('validates before saving', async function (this: ValidationModelTestContext, assert) {
    assert.strictEqual(
      typeof this.instance.save,
      'function',
      '.save is a function',
    );
    // trycatch because it will try a network request
    try {
      await this.instance.save().catch();
    } catch (e) {
      // do nothing
    }
    assert.strictEqual(
      this.validation.lastValidatedModel,
      this.instance,
      'The lastValidatedModel on the stub validation service is our model.',
    );
  });
});
