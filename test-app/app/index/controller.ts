import Controller from '@ember/controller';
import { action } from '@ember/object';
import CompanyModel from 'test-app/models/company';
import ContactModel from 'test-app/models/contact';

export default class IndexController extends Controller {
  declare company: CompanyModel;
  declare contact: ContactModel;

  @action
  save() {
    this.company.save().catch((e: any) => {
      console.error(e);
    });

    this.contact.save().catch((e: any) => {
      console.error(e);
    });
  }
}
