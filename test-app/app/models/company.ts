import { attr, hasMany, type SyncHasMany } from '@ember-data/model';
import ValidationModel from '@getflights/ember-attribute-validations/models/validation-model';
import { decimals } from '@getflights/ember-attribute-validations/validations/decimals';
import { negative } from '@getflights/ember-attribute-validations/validations/negative';
import { number } from '@getflights/ember-attribute-validations/validations/number';
import { positive } from '@getflights/ember-attribute-validations/validations/positive';
import { precision } from '@getflights/ember-attribute-validations/validations/precision';
import { range } from '@getflights/ember-attribute-validations/validations/range';
import { required } from '@getflights/ember-attribute-validations/validations/required';
import validation from '@getflights/ember-attribute-validations/decorators/validation';
import { maxLength } from '@getflights/ember-attribute-validations/validations/max-length';

export default class CompanyModel extends ValidationModel {
  @attr('string')
  @validation(required())
  declare name: string | undefined;

  @attr('string')
  @validation(maxLength(10))
  declare vatNumber: string | undefined;

  @attr('number')
  @validation(number(), range(0, 5))
  declare rating: number | undefined;

  @attr('number')
  @validation(number(), precision(2), decimals(1))
  declare score: number | undefined;

  @attr('number')
  @validation(number(), negative(), number())
  declare credit: number | undefined;

  @attr('number')
  @validation(number(), positive(), number())
  declare debit: number | undefined;

  @hasMany('contact', { async: false, inverse: 'company' })
  declare contacts: SyncHasMany<CompanyModel>;
}
