import { attr, belongsTo } from '@ember-data/model';
import ValidationModel from '@getflights/ember-attribute-validations/models/validation-model';
import { after } from '@getflights/ember-attribute-validations/validations/after';
import { before } from '@getflights/ember-attribute-validations/validations/before';
import { date } from '@getflights/ember-attribute-validations/validations/date';
import { email } from '@getflights/ember-attribute-validations/validations/email';
import { max } from '@getflights/ember-attribute-validations/validations/max';
import { min } from '@getflights/ember-attribute-validations/validations/min';
import { lengthBetween } from '@getflights/ember-attribute-validations/validations/length-between';
import { required } from '@getflights/ember-attribute-validations/validations/required';
import { wholenumber } from '@getflights/ember-attribute-validations/validations/wholenumber';
import validation from '@getflights/ember-attribute-validations/decorators/validation';
import type CompanyModel from './company';

export default class ContactModel extends ValidationModel {
  @attr('string')
  @validation(required(), lengthBetween(2, 20))
  declare firstName: string | undefined;

  @attr('string')
  @validation(required())
  declare lastName: string;

  @attr('string')
  @validation(email())
  declare email: string | undefined;

  @attr('number')
  @validation(wholenumber(), max(150), min(0))
  declare age: number | undefined;

  @attr('date')
  @validation(date(), after(new Date('2000-01-01')), before(() => new Date()))
  declare birthdate: Date | undefined;

  @belongsTo('company', { async: false, inverse: 'contacts' })
  @validation(required())
  declare company: CompanyModel | undefined;
}
