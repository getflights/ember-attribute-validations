import type { Validation } from '../interfaces/validation.ts';
import { decimalPlaces, isNone, isNumber } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';
import { assert } from '@ember/debug';

export function decimals(decimals: number): Validation {
  assert(
    'Missing argument for the `decimals` validation, which should be a number indicating the maximum amount of decimals.',
    !isNone(decimals),
  );

  assert(
    'Invalid argument for the `decimals` validation: not a valid number.',
    isNumber(decimals) && !isNaN(decimals),
  );

  return {
    name: 'decimals',
    test: (value) => {
      return new Promise((resolve, reject) => {
        // Only validate (valid) numbers
        if (isNumber(value) && !isNaN(value)) {
          if (decimalPlaces(value) > decimals) {
            return reject(new ValidationFailure({ decimals }));
          }
        }

        return resolve(true);
      });
    },
  };
}
