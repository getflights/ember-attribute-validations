import type { Validation } from '../interfaces/validation.ts';
import { assert } from '@ember/debug';
import {
  getCount,
  isBlankString,
  isNone,
  isNumber,
} from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';

export function maxLength(length: number): Validation {
  assert(
    'Missing argument for the `max-length` validation, which should be a number indicating the maximum length.',
    !isNone(length),
  );

  assert(
    'Invalid argument for the `max-length` validation: not a valid number.',
    isNumber(length) && !isNaN(length),
  );

  return {
    name: 'max-length',
    test: (value) => {
      return new Promise((resolve, reject) => {
        const count = getCount(value);

        // Only validate values that have a length
        if (!isNone(count) && !isBlankString(value)) {
          if (count > length) {
            return reject(new ValidationFailure({ length }));
          }
        }

        return resolve(true);
      });
    },
  };
}
