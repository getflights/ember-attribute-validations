import type { Validation } from '../interfaces/validation.ts';
import { isNone, isNumber } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';
import { assert } from '@ember/debug';

export function min(min: number): Validation {
  assert(
    'Missing argument for the `min` validation, which should be a number indicating the minimum.',
    !isNone(min),
  );

  assert(
    'Invalid argument for the `min` validation: not a valid number.',
    isNumber(min) && !isNaN(min),
  );

  return {
    name: 'min',
    test: (value) => {
      return new Promise((resolve, reject) => {
        // Only validate (valid) numbers
        if (isNumber(value) && !isNaN(value)) {
          if (value < min) {
            return reject(new ValidationFailure({ min }));
          }
        }

        return resolve(true);
      });
    },
  };
}
