import type { Validation } from '../interfaces/validation.ts';
import { hasValue } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';
import type { BelongsToValidation } from '../interfaces/belongs-to-validation.ts';

export function required(): Validation {
  return {
    name: 'required',
    test: (value) => {
      return new Promise((resolve, reject) => {
        if (!hasValue(value)) {
          return reject(new ValidationFailure());
        }

        return resolve(true);
      });
    },
  };
}

export function requiredId(): BelongsToValidation {
  const requiredValidation = required();

  return {
    ...requiredValidation,
    test: (ref, ...args) => {
      return requiredValidation.test(ref.id(), ...args);
    },
  };
}
