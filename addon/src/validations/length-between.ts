import type { Validation } from '../interfaces/validation.ts';
import { assert } from '@ember/debug';
import {
  getCount,
  isBlankString,
  isNone,
  isNumber,
} from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';

export function lengthBetween(from: number, to: number): Validation {
  assert(
    'Missing first argument for the `length-between` validation, which should be a number indicating the minimum length.',
    !isNone(from),
  );
  assert(
    'Invalid first argument for the `length-between` validation: not a valid number.',
    isNumber(from) && !isNaN(from),
  );
  assert(
    'Missing second argument for the `length-between` validation, which should be a number indicating the maximum length.',
    !isNone(to),
  );
  assert(
    'Invalid second argument for the `length-between` validation: not a valid number.',
    isNumber(to) && !isNaN(to),
  );

  return {
    name: 'length-between',
    test: (value) => {
      return new Promise((resolve, reject) => {
        const count = getCount(value);

        // Only validate values that have a length
        if (!isNone(count) && !isBlankString(value)) {
          if (count < from || count > to) {
            return reject(new ValidationFailure({ from, to }));
          }
        }

        return resolve(true);
      });
    },
  };
}
