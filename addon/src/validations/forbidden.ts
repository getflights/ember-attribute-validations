import type { Validation } from '../interfaces/validation.ts';
import { hasValue } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';
import type { BelongsToValidation } from '../interfaces/belongs-to-validation.ts';

export function forbidden(): Validation {
  return {
    name: 'forbidden',
    test: (value) => {
      return new Promise((resolve, reject) => {
        if (hasValue(value)) {
          return reject(new ValidationFailure());
        }

        return resolve(true);
      });
    },
  };
}

export function forbiddenId(): BelongsToValidation {
  const forbiddenValidation = forbidden();

  return {
    ...forbiddenValidation,
    test: (ref, ...args) => {
      return forbiddenValidation.test(ref.id(), ...args);
    },
  };
}
