import type { Validation } from '../interfaces/validation.ts';
import { isBoolean, isNone } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';

/**
 * Checks whether a value is a boolean
 * @argument
 */
export function boolean(strict = false): Validation {
  return {
    name: 'boolean',
    test: (value) => {
      return new Promise((resolve, reject) => {
        // Only validate values that are defined
        if (!isNone(value)) {
          const isBooleanNumber = value === 0 || value === 1;

          if (!(isBoolean(value) || (!strict && isBooleanNumber))) {
            return reject(new ValidationFailure());
          }
        }

        return resolve(true);
      });
    },
  };
}
