import type { Validation } from '../interfaces/validation.ts';
import { amountOfDigits, isNone, isNumber } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';
import { assert } from '@ember/debug';

export function precision(precision: number): Validation {
  assert(
    'Missing argument for the `precision` validation, which should be a number indicating the maximum precision.',
    !isNone(precision),
  );

  assert(
    'Invalid argument for the `precision` validation: not a valid number.',
    isNumber(precision) && !isNaN(precision),
  );

  return {
    name: 'precision',
    test: (value) => {
      return new Promise((resolve, reject) => {
        // Only validate (valid) numbers
        if (isNumber(value) && !isNaN(value)) {
          if (amountOfDigits(value) > precision) {
            return reject(new ValidationFailure({ precision }));
          }
        }

        return resolve(true);
      });
    },
  };
}
