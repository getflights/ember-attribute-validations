import { isNone } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';
import type { Validation } from '../interfaces/validation.ts';

/**
 * Checks whether a value is truthy
 */
export function acceptance(): Validation {
  return {
    name: 'acceptance',
    test: (value) => {
      return new Promise((resolve, reject) => {
        // Only validate values that are defined
        if (!isNone(value)) {
          if (!value) {
            return reject(new ValidationFailure());
          }
        }

        return resolve(true);
      });
    },
  };
}
