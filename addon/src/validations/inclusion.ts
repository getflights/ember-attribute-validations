import type { Validation } from '../interfaces/validation.ts';
import { assert } from '@ember/debug';
import { isNone } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';

export function inclusion(allowedValues: any[]): Validation;
export function inclusion<
  O extends object,
  F extends keyof O,
  V extends NoInfer<O[F]>,
>(allowedValues: V[]): Validation<O, F> {
  assert(
    'Missing argument for the `inclusion` validation, which should be an array of allowed values.',
    !isNone(allowedValues),
  );

  assert(
    'Invalid argument for the `inclusion` validation: must be an array.',
    Array.isArray(allowedValues),
  );

  assert(
    'Invalid argument for the `inclusion` validation: array must not be empty.',
    allowedValues.length > 0,
  );

  return {
    name: 'inclusion',
    test: (value) => {
      return new Promise((resolve, reject) => {
        // Only validate values that are defined
        if (!isNone(value)) {
          if (!allowedValues.includes(value)) {
            return reject(new ValidationFailure());
          }
        }

        return resolve(true);
      });
    },
  };
}
