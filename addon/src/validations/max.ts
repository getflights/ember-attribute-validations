import type { Validation } from '../interfaces/validation.ts';
import { isNone, isNumber } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';
import { assert } from '@ember/debug';

export function max(max: number): Validation {
  assert(
    'Missing argument for the `max` validation, which should be a number indicating the maximum.',
    !isNone(max),
  );

  assert(
    'Invalid argument for the `max` validation: not a valid number.',
    isNumber(max) && !isNaN(max),
  );

  return {
    name: 'max',
    test: (value) => {
      return new Promise((resolve, reject) => {
        // Only validate (valid) numbers
        if (isNumber(value) && !isNaN(value)) {
          if (value > max) {
            return reject(new ValidationFailure({ max }));
          }
        }

        return resolve(true);
      });
    },
  };
}
