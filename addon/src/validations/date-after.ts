import type { Validation } from '../interfaces/validation.ts';
import { assert } from '@ember/debug';
import type { DateFn } from '../-private/types.ts';
import { isDate, isValidDate, resolveDate } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';

export function dateAfter(dateOrDateFn: Date | DateFn): Validation {
  assert(
    'Missing argument for the `date-after` validation, which should be a Date or a function returning a Date.',
    dateOrDateFn,
  );

  assert(
    'Invalid argument for the `date-after` validation: not a date or function.',
    isDate(dateOrDateFn) || typeof dateOrDateFn === 'function',
  );

  return {
    name: 'date-after',
    test: (value) => {
      return new Promise((resolve, reject) => {
        const dateAfter = resolveDate(dateOrDateFn);
        assert(
          'The resolved date to validate against using `date-after` is not a valid date',
          isDate(dateAfter) && isValidDate(dateAfter),
        );

        // Only validate (valid) dates
        if (isDate(value) && isValidDate(value)) {
          if (value.getTime() <= dateAfter.getTime()) {
            return reject(new ValidationFailure({ date: String(dateAfter) }));
          }
        }

        return resolve(true);
      });
    },
  };
}
