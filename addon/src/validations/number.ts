import type { Validation } from '../interfaces/validation.ts';
import { isNone, isNumber } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';

export function number(): Validation {
  return {
    name: 'number',
    test: (value) => {
      return new Promise((resolve, reject) => {
        // only validate values that are defined
        if (!isNone(value)) {
          if (!isNumber(value) || isNaN(value) || !isFinite(value)) {
            return reject(new ValidationFailure());
          }
        }

        return resolve(true);
      });
    },
  };
}
