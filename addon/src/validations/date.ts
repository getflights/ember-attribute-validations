import type { Validation } from '../interfaces/validation.ts';
import { isDate, isNone, isValidDate } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';

export function date(): Validation {
  return {
    name: 'date',
    test: (value) => {
      return new Promise((resolve, reject) => {
        // Only validate values that are defined
        if (!isNone(value)) {
          if (!isDate(value) || !isValidDate(value)) {
            return reject(new ValidationFailure());
          }
        }

        return resolve(true);
      });
    },
  };
}
