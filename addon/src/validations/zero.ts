import type { Validation } from '../interfaces/validation.ts';
import { isNumber } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';

export function zero(): Validation {
  return {
    name: 'zero',
    test: (value) => {
      return new Promise((resolve, reject) => {
        // Only validate (valid) numbers
        if (isNumber(value) && !isNaN(value)) {
          if (Math.sign(value) !== 0) {
            return reject(new ValidationFailure());
          }
        }

        return resolve(true);
      });
    },
  };
}
