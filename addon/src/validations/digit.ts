import type { Validation } from '../interfaces/validation.ts';
import { pattern } from './pattern.ts';

export function digit(): Validation {
  const digitPattern = new RegExp('^\\d+$');
  const patternValidation = pattern(digitPattern);

  return {
    ...patternValidation,
    name: 'digit',
  };
}
