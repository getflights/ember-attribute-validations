import type { Validation } from '../interfaces/validation.ts';
import { assert } from '@ember/debug';
import { isNone, isNumber } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';

export function range(from: number, to: number): Validation {
  assert(
    'Missing first argument for the `range` validation, which should be a number indicating the minimum.',
    !isNone(from),
  );
  assert(
    'Invalid first argument for the `range` validation: not a valid number.',
    isNumber(from) && !isNaN(from),
  );
  assert(
    'Missing second argument for the `range` validation, which should be a number indicating the maximum.',
    !isNone(to),
  );
  assert(
    'Invalid second argument for the `range` validation: not a valid number.',
    isNumber(to) && !isNaN(to),
  );

  return {
    name: 'range',
    test: (value) => {
      return new Promise((resolve, reject) => {
        // Only validate (valid) numbers
        if (isNumber(value) && !isNaN(value)) {
          if (value < from || value > to) {
            return reject(new ValidationFailure({ from, to }));
          }
        }

        return resolve(true);
      });
    },
  };
}
