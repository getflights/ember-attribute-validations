import { assert } from '@ember/debug';
import { isBlankString, isNone, isNumber } from '../-private/utils.ts';
import type { Validation } from '../interfaces/validation.ts';
import ValidationFailure from '../-private/validation-failure.ts';

/**
 * Checks whether a value is a string or number that matches a specific pattern
 */
export function pattern<O = never, F = never>(
  pattern: RegExp,
): Validation<O, F> {
  assert(
    'Missing argument for the `pattern` validation, which should be a RegExp pattern.',
    !isNone(pattern),
  );

  assert(
    'Invalid argument for the `pattern` validation: not a RegExp.',
    pattern instanceof RegExp,
  );

  return {
    name: 'pattern',
    test: (value) => {
      return new Promise((resolve, reject) => {
        if (!isNone(value) && !isBlankString(value)) {
          if (
            !['string', 'number'].includes(typeof value) ||
            (isNumber(value) && isNaN(value)) ||
            !String(value).match(pattern)
          ) {
            return reject(new ValidationFailure());
          }
        }

        return resolve(true);
      });
    },
  };
}
