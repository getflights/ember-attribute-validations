import type { Validation } from '../interfaces/validation.ts';
import { isNumber } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';

export function wholenumber(): Validation {
  return {
    name: 'wholenumber',
    test: (value) => {
      return new Promise((resolve, reject) => {
        // Only validate (valid) numbers
        if (isNumber(value) && !isNaN(value)) {
          if (value % 1 !== 0) {
            return reject(new ValidationFailure());
          }
        }

        return resolve(true);
      });
    },
  };
}
