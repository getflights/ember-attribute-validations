import type { Validation } from '../interfaces/validation.ts';
import { isNumber } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';

export function positive(): Validation {
  return {
    name: 'positive',
    test: (value) => {
      return new Promise((resolve, reject) => {
        // Only validate (valid) numbers
        if (isNumber(value) && !isNaN(value)) {
          if (Math.sign(value) === -1) {
            return reject(new ValidationFailure());
          }
        }

        return resolve(true);
      });
    },
  };
}
