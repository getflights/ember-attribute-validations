import type { Validation } from '../interfaces/validation.ts';
import { assert } from '@ember/debug';
import type { DateFn } from '../-private/types.ts';
import { isDate, isValidDate, resolveDate } from '../-private/utils.ts';
import ValidationFailure from '../-private/validation-failure.ts';

export function dateBefore(dateOrDateFn: Date | DateFn): Validation {
  assert(
    'Missing argument for the `date-before` validation, which should be a Date or a function returning a Date.',
    dateOrDateFn,
  );

  assert(
    'Invalid argument for the `date-before` validation: not a date or function.',
    isDate(dateOrDateFn) || typeof dateOrDateFn === 'function',
  );

  return {
    name: 'date-before',
    test: (value) => {
      return new Promise((resolve, reject) => {
        const dateBefore = resolveDate(dateOrDateFn);
        assert(
          'The resolved date to validate against using `date-before` is not a valid date',
          isDate(dateBefore) && isValidDate(dateBefore),
        );

        // Only validate (valid) dates
        if (isDate(value) && isValidDate(value)) {
          if (value.getTime() >= dateBefore.getTime()) {
            return reject(new ValidationFailure({ date: String(dateBefore) }));
          }
        }

        return resolve(true);
      });
    },
  };
}
