import type { Validation } from '../interfaces/validation.ts';
import { pattern } from './pattern.ts';

/**
 * Checks whether a value is alphanumeric
 */
export function alphanumeric(): Validation {
  const alphanumericPattern = new RegExp('^[a-zA-Z0-9]*$');
  const patternValidation = pattern(alphanumericPattern);

  return {
    ...patternValidation,
    name: 'alphanumeric',
  };
}
