import { tracked } from '@glimmer/tracking';
import type DS from 'ember-data';

/**
 * Error thrown when a Model validation fails.
 *
 * This error contains the `DS.Errors` object from the Model.
 */
export default class ValidationError extends Error {
  @tracked errors: DS.Errors;
  @tracked message: string;

  constructor(message: string, errors: DS.Errors) {
    super(message);
    this.message = message;
    this.errors = errors;

    // @ts-ignore
    if (Error.captureStackTrace) {
      // @ts-ignore
      Error.captureStackTrace(this, ValidationError);
    }
  }
}
