import type Model from '@ember-data/model';
import Service from '@ember/service';
import { service } from '@ember/service';
import type Store from '@ember-data/store';
import type ModelRegistry from 'ember-data/types/registries/model';
import type IntlService from 'ember-intl/services/intl';
import { capitalize } from '@ember/string';
import type TransformRegistry from 'ember-data/types/registries/transform';
import type {
  ContextualValidation,
  ContextualValidationFunctionAPI,
  Validation,
} from '../interfaces/validation';
import { retrieveValidations } from '../decorators/validation.ts';
import ValidationFailure from '../-private/validation-failure.ts';
import { getOwner } from '@ember/owner';
import { retrieveBelongsToValidations } from '../decorators/belongs-to-validation.ts';

export type AttributeMeta = Parameters<
  Parameters<typeof Model.eachAttribute>[0]
>[1];
export type RelationshipMeta = Parameters<
  Parameters<typeof Model.eachRelationship>[0]
>[1];

export type ValidateModelOptions<M extends Model = any> = {
  skip?: (keyof M)[];
};

const INTL_CATEGORY = `ember-attribute-validations`;

export default class ValidationService extends Service {
  @service declare intl: IntlService;
  @service declare store: Store;

  /**
   * Return a list of validations for a given attribute (based on transform).
   * (THIS IS OVERWRITTEN MY MIST-COMPONENTS)
   * TODO: A more clean way to do this
   */
  validationsForTransformType<M extends Model, F extends keyof M>(
    _transformType: keyof TransformRegistry,
  ): (Validation | ContextualValidation<M, F>)[] {
    return [];
  }

  /**
   * Validate a single Attribute using the validations defined on the property (+ validations for the transform).
   * For each failed validation, error message is added to the Errors object for it's attribute name.
   */
  private async _validateAttribute<M extends Model, F extends keyof M>(
    model: M,
    attribute: AttributeMeta,
  ) {
    const transformValidations = this.validationsForTransformType<M, F>(
      attribute.type,
    );

    const validations = retrieveValidations<M, F>(model, attribute.name);

    const failures: [string, ValidationFailure][] = [];

    const api: ContextualValidationFunctionAPI<M, F> = {
      owner: getOwner(this)!,
      context: {
        target: model,
        key: attribute.name,
      },
    };

    const value = model[attribute.name];
    for (const validation of [...transformValidations, ...validations]) {
      await validation.test(value, api).catch((error) => {
        if (error instanceof ValidationFailure) {
          failures.push([validation.name, error]);
        } else {
          // Some other error occured while validating??
          this._addValidationException(
            model,
            attribute,
            validation.name,
            error,
          );
        }
      });
    }

    if (failures.length > 0) {
      for (const [validationName, failure] of failures) {
        this._addValidationError(model, attribute, validationName, failure);
      }
    }
  }

  /**
   * Validate a single Relationship using the validations defined on the property.
   * For each failed validation, error message is added to the Errors object for it's relationship name.
   */
  private async _validateRelationship<M extends Model, F extends keyof M>(
    model: M,
    relationship: RelationshipMeta,
  ) {
    const validations = retrieveValidations(model, relationship.key as F);

    const failures: [string, ValidationFailure][] = [];

    const api: ContextualValidationFunctionAPI<M, F> = {
      owner: getOwner(this)!,
      context: {
        target: model,
        key: relationship.key as F,
      },
    };

    if (validations.length > 0) {
      const value = model[relationship.key];

      for (const validation of validations) {
        await validation.test(value, api).catch((error) => {
          if (error instanceof ValidationFailure) {
            failures.push([validation.name, error]);
          } else {
            // Some other error occured while validating??
            this._addValidationException(
              model,
              relationship,
              validation.name,
              error,
            );
          }
        });
      }
    }

    if (relationship.kind === 'belongsTo') {
      const belongsToValidations = retrieveBelongsToValidations<M, F>(
        model,
        relationship.key,
      );

      const ref = model.belongsTo(relationship.key);
      for (const validation of belongsToValidations) {
        await validation.test(ref, api).catch((error) => {
          if (error instanceof ValidationFailure) {
            failures.push([validation.name, error]);
          } else {
            // Some other error occured while validating??
            this._addValidationException(
              model,
              relationship,
              validation.name,
              error,
            );
          }
        });
      }
    }

    if (failures.length > 0) {
      for (const [validationName, failure] of failures) {
        this._addValidationError(model, relationship, validationName, failure);
      }
    }
  }

  /**
   * Validate a list of attributes or relationships on the model, it will add errors on the Error object on the Model.
   * And return true if the fields are valid
   * @param model The Model you want to validate
   * @param fields What attributes or relationships you want to validate
   */
  public async validateFields<M extends Model, F extends keyof M & string>(
    model: M,
    fields: F[],
  ) {
    if (!fields || fields.length === 0) {
      return true;
    }

    // Map fields to an array of promises
    const valid = fields.map(async (field) => {
      return await this.validateField(model, field);
    });

    // Await all promises
    const validateResults = await Promise.all(valid);

    // None of them are falsy
    return !validateResults.includes(false);
  }

  /**
   * Validate an attribute or relationship on the model, it will add errors on the Error object on the Model.
   * And return true if the field is valid
   * @param model The Model you want to validate
   * @param field What attributes or fields you want to validate
   */
  public async validateField<M extends Model, F extends keyof M & string>(
    model: M,
    field: F,
  ) {
    // Do not validate the records which are deleted
    if (model.get('isDeleted')) {
      return true;
    }

    const modelClass = this.store.modelFor(
      (model.constructor as typeof Model).modelName as keyof ModelRegistry,
    ) as unknown as typeof Model;

    const errors = model.errors;
    errors.remove(field);

    /**
     * Search for this field in the attributes map
     */
    const attributes = modelClass.attributes as unknown as Map<
      string,
      AttributeMeta
    >;
    if (attributes.has(field)) {
      await this._validateAttribute(model, attributes.get(field)!);

      return !errors.has(field);
    }

    const relationships = modelClass.relationshipsByName as unknown as Map<
      string,
      RelationshipMeta
    >;
    if (relationships.has(field)) {
      // @ts-ignore because Ember Data types are wrong
      await this._validateRelationship(model, relationships.get(field)!.meta);

      return !errors.has(field);
    }

    return true;
  }

  private _attributeLabelFor(
    model: Model,
    attribute: AttributeMeta | RelationshipMeta,
  ): string {
    // @ts-ignore
    const modelName = model.constructor.modelName;
    const field = attribute.name;

    let label = '';
    if (
      this.intl.exists(`ember-field-components.${modelName}.fields.${field}`)
    ) {
      label = this.intl.t(
        `ember-field-components.${modelName}.fields.${field}`,
      );
    } else if (
      this.intl.exists(`ember-field-components.global.fields.${field}`)
    ) {
      label = this.intl.t(`ember-field-components.global.fields.${field}`);
    } else {
      label = capitalize(field);
    }

    return label;
  }

  intlFailureMessage(
    model: Model,
    attribute: AttributeMeta | RelationshipMeta,
    validationName: string,
    failure: ValidationFailure,
  ) {
    const intlKey = `${INTL_CATEGORY}.validation.${validationName}`;
    const label = this._attributeLabelFor(model, attribute);

    if (this.intl.exists(intlKey)) {
      return this.intl.t(intlKey, {
        label,
        ...failure.parameters,
      });
    }

    // Key does not exist
    let message = `${validationName}`;

    if (failure.parameters) {
      Object.entries(failure.parameters).forEach(([key, value]) => {
        message += ` (${key}: ${value})`;
      });
    }

    return message;
  }

  private _addValidationError(
    model: Model,
    attribute: AttributeMeta | RelationshipMeta,
    validation: string,
    failure: ValidationFailure,
  ) {
    const message = this.intlFailureMessage(
      model,
      attribute,
      validation,
      failure,
    );

    model.errors.add(attribute.name, message);
  }

  private _addValidationException(
    model: Model,
    attribute: AttributeMeta | RelationshipMeta,
    validation: string,
    error: unknown,
  ) {
    const label = this._attributeLabelFor(model, attribute);

    console.error(`Validation exception: ${String(error)}`);

    model.errors.add(
      attribute.name,
      this.intl.t(`${INTL_CATEGORY}.validation_exception`, {
        label,
        validation,
      }),
    );
  }

  /**
   * Validate a model (all attributes and relationships), it will add errors on the Error object on the Model.
   * And return true if the model is valid
   * @param model The Model you want to validate
   */
  async validateModel<M extends Model>(
    model: M,
    options: ValidateModelOptions<M> = {},
  ) {
    const skipAttributes = options.skip ? options.skip : [];

    // Do not validate the records which are deleted
    if (model.get('isDeleted')) {
      return true;
    }

    const modelClass = this.store.modelFor(
      (model.constructor as typeof Model).modelName as keyof ModelRegistry,
    ) as unknown as typeof Model;

    model.errors.clear();

    // Attributes
    const attributes = modelClass.attributes as unknown as Map<
      string,
      AttributeMeta
    >;
    for (const [key, attribute] of attributes) {
      // @ts-ignore
      if (skipAttributes.includes(key)) {
        continue;
      }

      await this._validateAttribute(model, attribute);
    }

    // Relationships
    const relationships = modelClass.relationshipsByName as unknown as Map<
      string,
      RelationshipMeta
    >;
    for (const [key, relationship] of relationships) {
      // @ts-ignore
      if (skipAttributes.includes(key)) {
        continue;
      }

      await this._validateRelationship(
        model,
        // @ts-ignore because Ember Data types are wrong
        relationship.meta as RelationshipMeta,
      );
    }

    // Return true if there are no errors
    return model.errors.get('isEmpty');
  }
  /**
   * Validate a list of models (all attributes and relationships), it will add errors on the Error objects on the Models.
   * And return true if ALL models are valid
   * @param models The Models you want to validate
   */

  async validateModels(models: Model[]) {
    // Map fields to an array of promises
    const valid = models.map(async (model) => {
      return await this.validateModel(model);
    });

    // Await all promises
    const validateResults = await Promise.all(valid);

    // Return true if none of then are falsy
    return !validateResults.includes(false);
  }
}
