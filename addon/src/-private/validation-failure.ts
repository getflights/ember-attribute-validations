export default class ValidationFailure {
  constructor(parameters?: Record<string, string | number>) {
    this.parameters = parameters;
  }

  public parameters: Record<string, string | number> | undefined;
}

export const isValidationFailure = (error: any): error is ValidationFailure => {
  return error instanceof ValidationFailure;
};
