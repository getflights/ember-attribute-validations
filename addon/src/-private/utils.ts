import type { DateFn } from './types';

/**
 * Checks whether the value is a boolean
 */
export function isBoolean(value: any): value is boolean {
  return typeof value === 'boolean';
}

/**
 * Returns the amount of items in an item (length or size)
 */
export function getCount(item: unknown) {
  if (isNone(item)) {
    return undefined;
  }

  if (typeof item === 'object') {
    let count;

    if ('length' in item!) {
      count = item.length;
    } else if ('size' in item!) {
      count = item.size;
    }

    if (typeof count === 'number') {
      return count;
    }
  } else if (typeof item === 'string') {
    return item.length;
  }

  return undefined;
}

/**
 * Checks whether the value is a date (instance)
 */
export function isDate(date: any): date is Date {
  return date instanceof Date;
}

/**
 * Checks whether the date instance is a valid date
 */
export function isValidDate(date: Date): boolean {
  return !isNaN(date.getTime());
}

/**
 * This function takes a date or function that returns a date and returns a date
 */
export function resolveDate(dateOrDateFn: DateFn | Date): Date | null {
  function isFn(dateOrDateFn: Date | DateFn): dateOrDateFn is DateFn {
    return typeof dateOrDateFn === 'function';
  }

  if (isFn(dateOrDateFn)) {
    dateOrDateFn = dateOrDateFn();
  }

  return dateOrDateFn;
}

/**
 * Checks whether a value is none (null or undefined)
 */
export function isNone(value: any): value is null | undefined {
  return value === null || value === undefined;
}

/**
 * Checks whether the value is a number
 */
export function isNumber(value: any): value is number {
  return typeof value === 'number';
}

/**
 * Returns the amount of digits after the decimal
 */
export function decimalPlaces(num: number): number {
  const match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
  if (!match) {
    return 0;
  }
  return Math.max(
    0,
    (match[1] ? match[1].length : 0) - (match[2] ? +match[2] : 0),
  );
}

/**
 * Returns the amount of digits
 */
export function amountOfDigits(num: number): number {
  num = Math.abs(num);
  return num.toString().replace('.', '').replace(',', '').length;
}

const notOnlyWhitespaceRegex = new RegExp('\\S');

export function isBlankString(value: any) {
  return (
    typeof value === 'string' && notOnlyWhitespaceRegex.test(value) === false
  );
}

/**
 * Determines whether or not a value is empty
 */
export function hasValue(value: any): boolean {
  // not null or undefined
  if (isNone(value)) {
    return false;
  }

  // not blank string
  if (isBlankString(value)) {
    return false;
  }

  // not empty
  const count = getCount(value);
  if (!isNone(count) && count < 1) {
    return false;
  }

  return true;
}
