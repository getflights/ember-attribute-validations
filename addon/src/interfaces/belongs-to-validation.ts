import type DS from 'ember-data';
import type { ValidationFunctionAPI } from './validation';
import type Owner from '@ember/owner';

export type BelongsToValidation<_O = never, _F = never> = {
  name: string;
  test: BelongsToValidationFunction;
};

export type BelongsToValidationFunction = (
  reference: DS.BelongsToReference,
  meta: ValidationFunctionAPI,
) => Promise<true>;

/**
 * Validation function with context
 */
export type ContextualBelongsToValidation<
  O extends object | unknown = unknown,
  F extends keyof O = keyof O,
> = {
  name: string;
  test: ContextualBelongsToValidationFunction<O, F>;
};

export type ContextualBelongsToValidationFunction<
  O extends object | unknown = unknown,
  F extends keyof O = keyof O,
> = (
  ref: DS.BelongsToReference,
  api: ContextualBelongsToValidationFunctionAPI<O, F>,
) => Promise<true>;

export type ContextualBelongsToValidationFunctionAPI<
  O extends object | unknown = unknown,
  F extends keyof O = keyof O,
> = {
  owner: Owner;
  context: {
    target: O;
    key: F;
  };
};
