import type Owner from '@ember/owner';

/**
 * Normal validation function
 */
export type Validation<_O = never, _F = never> = {
  name: string;
  test: ValidationFunction;
  parameters?: Record<string, string> | (() => Record<string, string>);
};

export type ValidationFunction = (
  value: any,
  api?: ValidationFunctionAPI,
) => Promise<true>;

export type ValidationFunctionAPI = {
  owner: Owner;
};

/**
 * Validation function with context
 */
export type ContextualValidation<
  O extends object | unknown = unknown,
  F extends keyof O = keyof O,
> = {
  name: string;
  test: ContextualValidationFunction<O, F>;
};

export type ContextualValidationFunction<
  O extends object | unknown = unknown,
  F extends keyof O = keyof O,
> = (value: any, api: ContextualValidationFunctionAPI<O, F>) => Promise<true>;

export type ContextualValidationFunctionAPI<
  O extends object | unknown = unknown,
  F extends keyof O = keyof O,
> = {
  owner: Owner;
  context: {
    target: O;
    key: F;
  };
};
