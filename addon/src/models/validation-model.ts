import Model from '@ember-data/model';
import { service } from '@ember/service';
import type ValidationService from '../services/validation.ts';
import type IntlService from 'ember-intl/services/intl';
import type RSVP from 'rsvp';
import { reject } from 'rsvp';
import ValidationError from '../error.ts';
import type { ValidateModelOptions } from '../services/validation.ts';

/**
 * TODO: This is a work in progress. The idea is to have a base class for all models that
 * have validations. This class will then override the save method to validate the model
 * before saving.
 */

export default class ValidationModel extends Model {
  @service declare intl: IntlService;
  @service declare validation: ValidationService;

  async validate(options?: ValidateModelOptions) {
    return await this.validation.validateModel(this, options);
  }

  /**
   * Override of the Save method of DS.Model
   * @param {*} param0 Options for the save method. Pass validate=false if you want to ignore validation
   */
  async save(
    options?:
      | {
          adapterOptions?: object | undefined;
          validate?: boolean;
          validationOptions?: ValidateModelOptions;
        }
      | undefined,
    // @ts-ignore
  ): RSVP.Promise<this> {
    if (options && options.validate === false) {
      return super.save(options);
    }

    if (await this.validate(options?.validationOptions)) {
      return super.save(options);
    }

    return reject(
      new ValidationError(
        this.intl.t('ember-attribute-validations.error'),
        this.errors,
      ),
    );
  }
}
