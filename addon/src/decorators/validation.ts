import 'reflect-metadata';

import type {
  ContextualValidation,
  Validation,
} from '../interfaces/validation';

// https://github.com/typestack/class-validator

export const METADATA_KEY = `eav_validations`;

export default function validation<O extends object, F extends keyof O>(
  ...validations: NoInfer<(Validation<O, F> | ContextualValidation<O, F>)[]>
) {
  return (target: O, propertyKey: F) => {
    registerValidations(target, propertyKey, validations);
  };
}

export function registerValidations<O extends object, F extends keyof O>(
  target: O,
  key: F,
  validations: (Validation | ContextualValidation<O, F>)[],
) {
  Reflect.defineMetadata(METADATA_KEY, validations, target, String(key));
}

export function retrieveValidations<M extends object, F extends keyof M>(
  target: M,
  propertyKey: F,
): (Validation | ContextualValidation<M, F>)[] {
  return Reflect.getMetadata(METADATA_KEY, target, String(propertyKey)) ?? [];
}
