import type {
  BelongsToValidation,
  ContextualBelongsToValidation,
} from '../interfaces/belongs-to-validation';

// https://github.com/typestack/class-validator

export const METADATA_KEY = `eav_belongsto_validations`;

export default function belongsToValidation<
  O extends object,
  F extends keyof O,
>(
  ...validations: NoInfer<
    (BelongsToValidation<O, F> | ContextualBelongsToValidation<O, F>)[]
  >
) {
  return (target: O, propertyKey: F) => {
    registerBelongsToValidations(target, propertyKey, validations);
  };
}

export function registerBelongsToValidations<
  O extends object,
  F extends keyof O,
>(
  target: O,
  key: F,
  validations: (BelongsToValidation | ContextualBelongsToValidation<O, F>)[],
) {
  Reflect.defineMetadata(METADATA_KEY, validations, target, String(key));
}

export function retrieveBelongsToValidations<
  O extends object,
  F extends keyof O,
>(
  target: O,
  propertyKey: F,
): (BelongsToValidation | ContextualBelongsToValidation<O, F>)[] {
  return Reflect.getMetadata(METADATA_KEY, target, String(propertyKey)) ?? [];
}
