// Add any types here that you need for local development only.
// These will *not* be published as part of your addon, so be careful that your published code does not rely on them!

/**
 * GLINT
 */
import '@glint/environment-ember-loose';
import '@glint/environment-ember-template-imports';

declare module '@glint/environment-ember-loose/registry' {
  export default interface Registry {
    // local entries
  }
}

/**
 * EMBER DATA (CATCH-ALL)
 */
// Model Registry
declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    [key: string]: unknown;
  }
}
